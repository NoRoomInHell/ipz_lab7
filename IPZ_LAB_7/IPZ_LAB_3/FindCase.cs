﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ_LAB_3
{

    class FindCase
    {


        // NewCase ns;

        public void GetAllVal(long foo)
        {
            NewCase ns = NewCase.NCDic[foo];
            Console.WriteLine("This information only in current Case that has CaseID: " + foo);
            Console.WriteLine();
            foreach (Witnesses w in ns.Witnesse_List)
            {
                w.ShowFullInfo();
            }
            foreach (Suspect s in ns.Suspect_List)
            {
                s.ShowFullInfo();
            }
            foreach (Evidence e in ns.Evidence_List)
            {
                e.ShowFullInfo();
            }
        }

        public void GetAllVal()
        {
            Console.WriteLine("Full information in all Cases: ");
            Console.WriteLine();
            foreach (KeyValuePair<long, NewCase> cs in NewCase.NCDic)
            {
                foreach (Witnesses w in cs.Value.Witnesse_List)
                {
                    w.ShowFullInfo();

                }
                foreach (Suspect s in cs.Value.Suspect_List)
                {
                    s.ShowFullInfo();

                }
                foreach (Evidence e in cs.Value.Evidence_List)
                {
                    e.ShowFullInfo();

                }
            }
        }
        public void GetAllEvidence()
        {
            foreach (KeyValuePair<long, NewCase> cs in NewCase.NCDic)
            {
                foreach (Evidence w in cs.Value.Evidence_List)
                {
                    w.ShowFullInfo();
                    Console.WriteLine("Evidence's CaseID is: " + w.currentIdCase);

                }
            }
        }
        public void GetAllVal(Witnesses needToFind)
        {

            foreach (KeyValuePair<long, NewCase> cs in NewCase.NCDic)
            {

                cs.Value.Witnesse_List.FindAll(delegate(Witnesses ws)
                {

                    if (ws.firstName == needToFind.firstName)
                    {
                        Console.WriteLine("Have Witnesse with same First name: " + ws.firstName + " His CaseID is : " + ws.currentIdCase);

                    }
                    if (ws.lastName == needToFind.lastName)
                    {
                        Console.WriteLine("Have Witnesse with same  Last name: " + ws.firstName + " His CaseID is : " + ws.currentIdCase);

                    }
                    if (ws.firstName == needToFind.firstName && ws.firstName == needToFind.firstName)
                    {
                        Console.WriteLine("Have Witnesse with same Names: " + ws.firstName + " His CaseID is : " + ws.currentIdCase);

                    }
                    else
                    {

                        return false;
                    }
                    return true;
                }
               );
            }

        }
        public void GetAllVal(Suspect needToFind)
        {

            foreach (KeyValuePair<long, NewCase> cs in NewCase.NCDic)
            {

                cs.Value.Suspect_List.FindAll(delegate(Suspect ws)
                {
                    if (ws.lastName == needToFind.lastName)
                    {
                        Console.WriteLine("Suspect with same Last Name: " + ws.lastName + " His CaseID is : " + ws.currentIdCase);

                    }
                    if (ws.firstName == needToFind.firstName)
                    {
                        Console.WriteLine("Suspect with same First Name: " + ws.firstName + " His CaseID is : " + ws.currentIdCase);

                    }

                    //if (ws.addres == needToFind.addres)
                    //{
                    //    Console.WriteLine("Suspect with same City: " + ws.firstName.FirstName + " " + ws.firstName.LastName + " His CaseID is : " + ws.currentIdCase);

                    //}
                    //if (ws.addres.City == needToFind.addres.City && ws.addres.Street == needToFind.addres.Street)
                    //{
                    //    Console.WriteLine("Suspect with same City and Street: " + ws.firstName.FirstName + " " + ws.firstName.LastName + " His CaseID is : " + ws.currentIdCase);
                    //    if (ws.addres.City == needToFind.addres.City && ws.addres.Street == needToFind.addres.Street && ws.addres.House == needToFind.addres.House)
                    //        Console.WriteLine("Suspect have same Full Address: " + ws.firstName.FirstName + " " + ws.firstName.LastName + " His CaseID is : " + ws.currentIdCase);


                    //}
                    else
                    {

                        return false;
                    }
                    return true;
                }
               );
            }

        }
        }
    }


