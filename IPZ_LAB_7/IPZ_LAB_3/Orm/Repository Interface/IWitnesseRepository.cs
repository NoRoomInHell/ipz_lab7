﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
namespace IPZ_LAB_3.Orm
{
  public   interface IWitnesseRepository: IRepository<Witnesses>
    {
      IQueryable<Witnesses> SearchWitnessesByFirstName(string name);
      IQueryable<Witnesses> SearchWitnessesByLastName(string lastName);
      IQueryable<Witnesses> SearchWitnesseByFullName(string name, string lastName);
    }
}
