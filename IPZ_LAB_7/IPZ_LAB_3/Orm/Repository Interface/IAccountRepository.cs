﻿using System;
using IPZ_LAB_3;
using System.Linq;
namespace IPZ_LAB_3.Orm
{
   public interface IAccountRepository:IRepository<Account>
    {
       IQueryable<Account> FindById(long id);
       IQueryable<Account> FindByPassword(string password);
       
  
    }
}
