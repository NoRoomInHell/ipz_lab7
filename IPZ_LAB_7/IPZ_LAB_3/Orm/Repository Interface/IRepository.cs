﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
namespace IPZ_LAB_3.Orm
{
  public  interface IRepository<T> where T: class
    {
        T Load(string name);
        T LoadById(long id);
        T AttachByID(long id);
        IQueryable<T> LoadAll();

        void Add(T t);
        void DeleteAll();
        void Remove(T t);
        void Commit(
            );
    }
}
