﻿using System;
using System.Collections.Generic;
using System.Linq;
using IPZ_LAB_3;
namespace IPZ_LAB_3.Orm
{
  public  interface IAccountDataRepository:IRepository<AccountData>
    {
      IQueryable<AccountData> FindByPassword(string password);
    }
}
