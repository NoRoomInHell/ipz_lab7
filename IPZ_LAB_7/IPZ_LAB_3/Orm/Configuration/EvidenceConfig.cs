﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
namespace IPZ_LAB_3.Orm
{
    class EvidenceConfig: EntityTypeConfiguration<Evidence>
    {
        public EvidenceConfig()
        {
            
            HasKey(f => f.currentIdCase);
            Property(f => f.evidenceType);
            Property(f => f.information);
            Property(f => f.urlPhoto);
            Property(f => f.linkId);
        }
    }
}
