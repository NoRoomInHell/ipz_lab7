﻿using System;
using System.Data.Entity.ModelConfiguration;
namespace IPZ_LAB_3.Orm
{
    class AccountConfiguration:EntityTypeConfiguration<Account>
    {
        public AccountConfiguration()
        {
            HasKey(a => a.password);
            Property(a=>a.Name);
            Property(a=>a.LastName);
            Property(a => a.myID);
        }
    }
}
