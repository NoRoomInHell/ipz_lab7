﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
namespace IPZ_LAB_3.Orm
{
    class NameConfiguration:EntityTypeConfiguration<Name>

    {
       public NameConfiguration()
        {
            HasKey(p => p.ID);
             Property(p => p.LastName); 
            Property(p => p.FirstName);
            
       }

    }
}
