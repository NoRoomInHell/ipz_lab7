﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace IPZ_LAB_3.Orm
{
    class AccountDataConfiguration:EntityTypeConfiguration<AccountData>
    {
        public AccountDataConfiguration()
        {
            HasKey(p => p.password);
            Property(p => p.myName);
            Property(p => p.myLastName);
        }
    }
}
