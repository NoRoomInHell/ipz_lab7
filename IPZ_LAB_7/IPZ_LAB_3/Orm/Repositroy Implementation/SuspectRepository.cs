﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
namespace IPZ_LAB_3.Orm
{
    class SuspectRepository:BasicRepository<Suspect>,ISuspectRepositroy
    {

        public SuspectRepository(ProkDbContext dbContext)
            : base(dbContext, dbContext.Sus)
        { }

        public IQueryable<Suspect> SearchSuspectByFirstName(string name)
        {
            var db = GetDBContext();
            return db.Sus.Where(p => p.firstName == name);

        }
        public IQueryable<Suspect> SearchSuspectByLastName(string lastName)
        {
            var db = GetDBContext();
            return db.Sus.Where(p => p.lastName == lastName);

        }
        public IQueryable<Suspect> SearchSuspectByFullName(string name, string lastName)
        {
            var db = GetDBContext();
            return db.Sus.Where(p => p.firstName == name && p.lastName == lastName);
        }
       
    }
}
