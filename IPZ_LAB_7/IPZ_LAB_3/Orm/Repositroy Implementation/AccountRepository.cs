﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ_LAB_3.Orm
{
    class AccountRepository:BasicRepository<Account>,IAccountRepository
    {
        public AccountRepository(ProkDbContext dbContext)
            :base(dbContext,dbContext.Account)
        { }
       public IQueryable<Account> FindByPassword(string password)
       {
           var db = GetDBContext();
           return db.Account.Where(p => p.password == password);
       }
        public IQueryable<Account> FindById(long id)
       {
           var db = GetDBContext();
           return db.Account.Where(p => p.myID == id);
       }
       
    }
}
