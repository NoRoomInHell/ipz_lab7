﻿using System;
using System.Collections.Generic;
using System.Linq;
using IPZ_LAB_3;
namespace IPZ_LAB_3.Orm
{
    class AccountDataRepositorycs:BasicRepository<AccountData>,IAccountDataRepository
    {
        public AccountDataRepositorycs(ProkDbContext dbContext)
            :base(dbContext,dbContext.AccData)
        { }
    public   IQueryable<AccountData> FindByPassword(string password)
        {
            var db = GetDBContext();
            return db.AccData.Where(p => p.password == password);
        }
    }
}
