﻿using System;
using IPZ_LAB_3;
using System.Linq;

namespace IPZ_LAB_3.Orm
{
   class EvidenceRepository:BasicRepository<Evidence>,IEvidenceRepository
    {
       public EvidenceRepository(ProkDbContext dbContext)
           :base(dbContext,dbContext.Evidence)
       { }

       
       public IQueryable<Evidence> FindByID(long caseID)
       {
           var db = GetDBContext();
           
               return db.Evidence.Where(p =>p.linkId == caseID );
           
       }
       

    }
}
