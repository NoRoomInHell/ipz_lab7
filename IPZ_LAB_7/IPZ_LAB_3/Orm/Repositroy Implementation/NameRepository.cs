﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
namespace IPZ_LAB_3.Orm
{
    class NameRepository:BasicRepository<Name>,INameRepository
    {
       
        public NameRepository (ProkDbContext dbContext)
            :base(dbContext,dbContext.Name)
        { }
    
    public IQueryable<Name> FindByName(string name)
        {
            var db = GetDBContext();
            
            return db.Name.Where(t => t.FirstName == name);
        }
    
    }
}
