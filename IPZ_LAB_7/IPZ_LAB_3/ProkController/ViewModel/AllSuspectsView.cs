﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IPZ_LAB_3.ProkController.ViewModel
{
  public  class AllSuspectsView:BasicViewModel<AllSuspectsView>
    {
        public ICollection<SuspectView> suspects { get; set; }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object>();
            
            list.AddRange(suspects);
            return list;
        }
        public override string ToString()
        {
            return string.Format("Suspects: \n{0}\n",
            string.Join("\n", suspects)
                );
        }

    }
}
