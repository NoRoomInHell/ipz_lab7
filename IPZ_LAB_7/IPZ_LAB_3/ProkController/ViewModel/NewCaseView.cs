﻿using System;
using System.Collections.Generic;

namespace IPZ_LAB_3.ProkController.ViewModel
{
  public  class NewCaseView:BasicViewModel<NewCaseView>
    {
        public ICollection<WitnesseView> witnesse{get;set;}

        public  ICollection <SuspectView> suspect{ get; set; }
        public long ID { get; set; }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object> { ID };
            list.AddRange(witnesse);
            list.AddRange(suspect);
            return list;
        }
        public override string ToString()
        {
            return string.Format("ID = {0}\nWitnesse: \n{1}\nSuspect: \n{2}\n",
            ID,
            string.Join("\n",witnesse),
            string.Join("\n",suspect)
                );
        }
    }
}
