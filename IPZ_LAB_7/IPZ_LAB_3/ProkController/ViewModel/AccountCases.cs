﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ_LAB_3.ProkController.ViewModel
{
   public class AccountCases:BasicViewModel<AccountCases>
       
    {
       public ICollection<NewCaseView> newcase { get; set; }
       public string Name { get; set; }
       public string LastName { get; set; }

       protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
       {
           var list = new List<object>{Name,LastName};

           list.AddRange(newcase);
           return list;
       }
       public override string ToString()
       {
           return string.Format("Hello  {0}  {1}\nYour Cases : \n{2}\n",
            Name,
            LastName,
            string.Join("\n", newcase)
            
                );
       }
    }
}
