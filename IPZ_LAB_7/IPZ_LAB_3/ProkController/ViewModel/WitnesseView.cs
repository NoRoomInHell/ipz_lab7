﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IPZ_LAB_3.ProkController.ViewModel
{
    public class WitnesseView: BasicViewModel<WitnesseView>
    {
        public string Name { get; set; }
        public string LastName { get; set; }

        public long ID { get; set; }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object> { Name, LastName,ID };
            return list;
        }
        public override string ToString()
        {
            return string.Format("Name = {0}\nLastName={1}\nID = {2}\n",
            Name,
            LastName,
            ID
                );
        }
    }
   
}
