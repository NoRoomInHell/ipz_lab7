﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IPZ_LAB_3.ProkController.ViewModel
{
  public   class AllWitnesseView:BasicViewModel<AllWitnesseView>
    {
        public ICollection<WitnesseView> witnesses { get; set; }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object>();

            list.AddRange(witnesses);
            return list;
        }
        public override string ToString()
        {
            return string.Format("Witneses: \n{0}\n",
            string.Join("\n", witnesses)
                );
        }
    }
}
