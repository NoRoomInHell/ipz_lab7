﻿using System;
using System.Collections.Generic;
using System.Linq;
using IPZ_LAB_3.ProkController.ViewModel;
namespace IPZ_LAB_3.ProkController
{
    static class ViewModelFactory
    {
        static internal WitnesseView BuildWitnesseView(IPZ_LAB_3.Witnesses wit)
        {
            return new ViewModel.WitnesseView
            {
                Name = wit.firstName,
                LastName = wit.lastName,
                ID = wit.currentIdCase
            };
        }
        static internal AccountCases BuildAccCasesView(Account acc)
        {
            return new ViewModel.AccountCases
            {
                Name = acc.Name,
                LastName = acc.LastName,
                 newcase= BuildViewModels(acc.myCases.AsQueryable(), BuildNewCaseView)
            };
        }
        static internal AllWitnesseView BuildAllWitnessesView(Witnesses[] wit)
        {
            return new ViewModel.AllWitnesseView
            {

                witnesses= BuildViewModels(wit.AsQueryable(), BuildWitnesseView)
            };
        }
        
        static internal SuspectView BuildSuspectView(IPZ_LAB_3.Suspect sus)
        {
            return new ViewModel.SuspectView
            {
                Name = sus.firstName,
                LastName = sus.lastName,
                ID = sus.currentIdCase
            };
        }
        static internal AllSuspectsView BuildAllSuspectView(Suspect[] sus)
        {
            return new ViewModel.AllSuspectsView
            {

                suspects = BuildViewModels(sus.AsQueryable(), BuildSuspectView)
            };
        }
        static internal NewCaseView BuildNewCaseView(NewCase cas)
        {
            return new ViewModel.NewCaseView
            {
               ID =cas.currentIdCase,
               witnesse = BuildViewModels(cas.Witnesse_List.AsQueryable(),BuildWitnesseView),
                suspect = BuildViewModels(cas.Suspect_List.AsQueryable(),BuildSuspectView)
            };
        }
        static internal ICollection<TView> BuildViewModels<TView, TEntity>(
         IQueryable<TEntity> query,
         Func<TEntity, TView> converter
     )
        {
            var results = new List<TView>();
            foreach (TEntity entity in query)
                results.Add(converter(entity));
            return results;
        }

        }
}
