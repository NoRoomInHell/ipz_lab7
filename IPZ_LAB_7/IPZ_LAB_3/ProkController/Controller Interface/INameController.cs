﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
namespace IPZ_LAB_3.ProkController
{
   public interface INameController:IDisposable
    {
       Name[] GetAllNames();
       Name[] FindByName(string name);
       void CreateName(string firstName,string lastName);
    }
}
