﻿using System;
using System.Linq;
using IPZ_LAB_3;
using IPZ_LAB_3.ProkController.ViewModel;
namespace IPZ_LAB_3.ProkController
{
    public interface IAccountController:IDisposable
    {
      
        AccountCases SignIn(string password);
         
    }
}
