﻿using System;
using System.Collections.Generic;
using System.Linq;
using IPZ_LAB_3;
using IPZ_LAB_3.ProkController.ViewModel;
namespace IPZ_LAB_3.ProkController
{
 public    interface IAccountDataController:IDisposable
    {
     void CreateAccount(string password, string name, string lastName);

    }
}
