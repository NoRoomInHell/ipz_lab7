﻿using System;
using IPZ_LAB_3;
using IPZ_LAB_3.ProkController.ViewModel;
namespace IPZ_LAB_3.ProkController
{
   public interface ISuspectController: IDisposable
    {
        
        SuspectView FindSuspect(string name, string lastName);
        AllSuspectsView GetAllSuspect();
       SuspectView FindSuspectByID(long ID);
        void CreateNewSuspect(string name, string lastname);
        void Rename( string fisrtName,string lastName,long id);
        void Delete(long id);
        void DeleteAllSuspects(); 
      
        
    }
}
