﻿using System;
using IPZ_LAB_3;
using IPZ_LAB_3.ProkController.ViewModel;
namespace IPZ_LAB_3.ProkController
{
    public interface IWitnesseController: IDisposable
    {
        AllWitnesseView GetAllWitnesses();
        WitnesseView FindWitnesseByID(long ID);
        WitnesseView FindWitnesse(string name,string lastName);
        
        void CreateNewWitnesse(string name, string lastname);
        void Rename(long id, string newName,string newLastName);
        void Delete(long id);
        void DeleteAllWitnesse();    
    }
}
