﻿using System;
using IPZ_LAB_3;
using IPZ_LAB_3.ProkController.ViewModel;
namespace IPZ_LAB_3.ProkController
{
  public  interface ICaseController:IDisposable
    {
      void CreateCase(string password);

      NewCaseView OpenCase(long id);

      NewCase[] GetAllCases();

      void AddWitnesse(string firstName,string lastName,long id);
      void AddWitnesseFrom( long id,long witnesseId);
      void AddSuspect(string firstName, string lastName, long id);
      void AddSuspectFrom(long id, long suspectId);
      void AddEvidence(string info, string typeDetail, string url,long id);
      void AddEvidenceFrom(long id, long evidenceId);
  }
}
