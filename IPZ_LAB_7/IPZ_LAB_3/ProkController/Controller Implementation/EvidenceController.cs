﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;
namespace IPZ_LAB_3.ProkController
{
    class EvidenceController:BasicController,IEvidenceController
    {
        public IEvidenceRepository evidenceRepository;
        public EvidenceController()
        {
            this.evidenceRepository = RepositoryFactory.MakeEvidenceRepository(GetDBContext());
        }
        public Evidence FindByID(long ID)
        {
            return evidenceRepository.LoadById(ID);
        }
       
        public Evidence[] GetAllEvidence()
        {
            return evidenceRepository.LoadAll().ToArray();
        }
        public void SearchEvidenceByID(long id)
        {
            evidenceRepository.FindByID(id);
        }
        public void CreateEvidence(string info,string detailType,string urlPhoto)
        {
            Evidence ev = new Evidence();
            ev.information = info;
            ev.evidenceType = detailType;
            ev.urlPhoto = urlPhoto;
            evidenceRepository.Add(ev);
            evidenceRepository.Commit();
        }
        public void Change(long id,string info,string detailType,string urlPhoto)
        {

            evidenceRepository.LoadById(id).information = info;
            evidenceRepository.LoadById(id).evidenceType = detailType;
            evidenceRepository.LoadById(id).urlPhoto = urlPhoto;
            evidenceRepository.Commit();
                    
        }
        public void Delete(long id)
        {
            var db = evidenceRepository.LoadById(id);
            evidenceRepository.Remove(db);
            evidenceRepository.Commit();
        }
        public void DeleteAllEvidence()
        {
            evidenceRepository.DeleteAll();
            evidenceRepository.Commit();
        }

    }
}
