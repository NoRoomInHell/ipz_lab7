﻿using System;
using System.Collections.Generic;
using System.Linq;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;
using IPZ_LAB_3.ProkController.ViewModel;
namespace IPZ_LAB_3.ProkController
{
    class AccountController : BasicController, IAccountController
    {
        private IAccountRepository accountRepository;
        private IAccountDataRepository accountDataRepositroy;
        public AccountController()
        {
            this.accountRepository = RepositoryFactory.MakeAccountRepositroy(GetDBContext());
            this.accountDataRepositroy = RepositoryFactory.MakeAccountDataRepositroy(GetDBContext());
        }

        public AccountCases SignIn(string password)
        {

            Account ap = new Account();
            var db = accountDataRepositroy.Load(password);
            var ss = accountRepository.Load(password);
            if (ss == null && db != null)
            {
                ap.Name = db.myName;
                ap.LastName = db.myLastName;
                ap.password = db.password;
                Account.currentPassword = password;
                Console.WriteLine("New User added !");
                Console.WriteLine(ap.Name + " "+ ap.LastName + " ");
                accountRepository.Add(ap);
                accountRepository.Commit();
                //return ViewModelFactory.BuildAccCasesView(ap);
            }
            else if(ss != null)
            {
                return ViewModelFactory.BuildAccCasesView(ss);
            }
            else if(db == null)
                throw new SystemException("INCORRECRT PASSWORD");
            return null;

        }
        
        
        
        

    }

}