﻿using System;
using System.Collections.Generic;
using System.Linq;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;
using IPZ_LAB_3.ProkController.ViewModel;
namespace IPZ_LAB_3.ProkController
{
    class AccountDataController:BasicController,IAccountDataController
    {
        private IAccountDataRepository dataRepository;
        public AccountDataController()
        {
            this.dataRepository = RepositoryFactory.MakeAccountDataRepositroy(GetDBContext());
        }
        public void CreateAccount(string password, string name, string lastName)
        {
            AccountData dp = new AccountData();
            dp.password = password;
            dp.myName = name;
            dp.myLastName = lastName;
            dp.AddToData();
            dataRepository.Add(dp);
            dataRepository.Commit();
        }

    }
}
