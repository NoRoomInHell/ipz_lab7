﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;
using IPZ_LAB_3.ProkController.ViewModel;
namespace IPZ_LAB_3.ProkController
{
    class SuspectController:BasicController,ISuspectController
    {
            private ISuspectRepositroy suspectRepository;
      
        public SuspectController()
        {
            this.suspectRepository = RepositoryFactory.MakeSuspectRepositroy(GetDBContext());

        }
        public AllSuspectsView GetAllSuspect()
   {
       var sus = suspectRepository.LoadAll().ToArray();

       return ViewModelFactory.BuildAllSuspectView(sus);
   }

        
      
       public SuspectView FindSuspectByID(long ID)
       {
           var suspect = suspectRepository.LoadById(ID);
           return ViewModelFactory.BuildSuspectView(suspect);
       }
       public SuspectView FindSuspect(string name, string lastName)
       {
           Suspect[] suspectFullName = suspectRepository.SearchSuspectByFullName(name, lastName).ToArray();


           foreach (Suspect z in suspectFullName)
           {


               return ViewModelFactory.BuildSuspectView(z);

           }
           if (suspectFullName.Length == 0)
           {
               Console.WriteLine("Suspect with full name like this not found\n Trying to find suspects with same LASTNAME");
               Suspect[] suspectLastName = suspectRepository.SearchSuspectByLastName(lastName).ToArray();
               foreach (Suspect s in suspectLastName)
               {
                   return ViewModelFactory.BuildSuspectView(s);
               }
               if (suspectLastName.Length == 0)
               {
                   Console.WriteLine("Suspect with LASTNAME like this not found\n Trying to find suspects with same FIRSTNAME");
                   Suspect[] suspectFirstName = suspectRepository.SearchSuspectByFirstName(name).ToArray();
                   foreach (Suspect p in suspectFirstName)
                   {
                       return ViewModelFactory.BuildSuspectView(p);
                   }
               }
           }
           Console.WriteLine("SUSPECT NOT FOUND");
           return null;
       }
        public void CreateNewSuspect(string firstName,string lastName)
        {
            IPZ_LAB_3.Suspect ws = new IPZ_LAB_3.Suspect();
            ws.firstName = firstName;
            ws.lastName = lastName;

            suspectRepository.Add(ws);
            suspectRepository.Commit();
            
        }
       public void Rename( string firstName,string lastName,long id)
        {
            suspectRepository.LoadById(id).firstName = firstName;
            suspectRepository.LoadById(id).lastName = lastName; 
           suspectRepository.Commit();
        }
       public void Delete(long id)
       {
           var db = suspectRepository.LoadById(id);
           suspectRepository.Remove(db);
           suspectRepository.Commit();
       }
       public void DeleteAllSuspects()
       {
          suspectRepository.DeleteAll();
           suspectRepository.Commit();
       }
    

    }
}
