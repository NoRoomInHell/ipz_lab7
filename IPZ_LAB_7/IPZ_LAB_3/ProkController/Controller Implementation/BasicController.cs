﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3.Orm;
namespace IPZ_LAB_3.ProkController
{
    
    class BasicController
    {
        private ProkDbContext dbContext;
      protected ProkDbContext GetDBContext()
      {
          if(dbContext == null)
          dbContext = new ProkDbContext();
          return dbContext;

      }
        public void Dispose()
      {
          Dispose(true);
          GC.SuppressFinalize(this);
      }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && dbContext != null)
                dbContext.Dispose();
        }

    }
}
