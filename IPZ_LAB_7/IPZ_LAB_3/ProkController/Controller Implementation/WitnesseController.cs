﻿using System;
using System.Linq;
using System.Text;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;
using IPZ_LAB_3.ProkController.ViewModel;
namespace IPZ_LAB_3.ProkController
{

   class WitnesseController : BasicController, IWitnesseController
    {
        private IWitnesseRepository witnesseRepository;
      
        public WitnesseController()
        {
            this.witnesseRepository = RepositoryFactory.MakeWitnesseRepository(GetDBContext());

        }
       public WitnesseView FindWitnesseByID(long ID)
        {
            var wintesse = witnesseRepository.LoadById(ID);
            return ViewModelFactory.BuildWitnesseView(wintesse);
        }
       public WitnesseView FindWitnesse(string name,string lastName)
        {
           Witnesses[] witnesseFullName = witnesseRepository.SearchWitnesseByFullName(name,lastName).ToArray();
           
           
               foreach (Witnesses z in witnesseFullName)
               {

                   
                   return ViewModelFactory.BuildWitnesseView(z);
                       
               }
               if (witnesseFullName.Length == 0)
               {
                   Console.WriteLine("Witnesse with full name like this not found\n Trying to find witnesses with same LASTNAME");
                   Witnesses[] witnesseLastName = witnesseRepository.SearchWitnessesByLastName(lastName).ToArray();
                        foreach(Witnesses s in witnesseLastName)
                        {
                            return ViewModelFactory.BuildWitnesseView(s);
                        }
                   if(witnesseLastName.Length == 0)
                   {
                       Console.WriteLine("Witnesse with LASTNAME like this not found\n Trying to find witnesses with same FIRSTNAME");
                       Witnesses[] witnesseFirstName = witnesseRepository.SearchWitnessesByFirstName(name).ToArray();
                       foreach(Witnesses p in witnesseFirstName)
                       {
                           return ViewModelFactory.BuildWitnesseView(p);
                       }
                       }
               }  
               Console.WriteLine("WITNESSE NOT FOUND");
               return null;
        }
       public AllWitnesseView GetAllWitnesses()
   {
       var wit = witnesseRepository.LoadAll().ToArray();

       return ViewModelFactory.BuildAllWitnessesView(wit);
   }

        
       public Witnesses FindWitnesseByName(string name)
        {
          return  witnesseRepository.Load(name);

        }
        public void CreateNewWitnesse(string firstName,string lastName)
        {
            IPZ_LAB_3.Witnesses ws = new IPZ_LAB_3.Witnesses();
            ws.firstName = firstName;
            ws.lastName = lastName;

            witnesseRepository.Add(ws);
            witnesseRepository.Commit();
            
        }
       public void Rename(long id, string newName,string lastName)
        {
            witnesseRepository.LoadById(id).firstName = newName;
            witnesseRepository.LoadById(id).lastName = lastName;
            witnesseRepository.Commit(); 
       }
       
       public void Delete(long id)
       {
           var db = witnesseRepository.LoadById(id);
           witnesseRepository.Remove(db);
           witnesseRepository.Commit();
       }
       public void DeleteAllWitnesse()
       {
           witnesseRepository.DeleteAll();
           witnesseRepository.Commit();
       }
    
    }
}
