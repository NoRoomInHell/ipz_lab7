﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;
using IPZ_LAB_3.ProkController.ViewModel;
namespace IPZ_LAB_3.ProkController
{
    class CaseController:BasicController,ICaseController
    {

       private IMenuRepository menuRepository;
       private IWitnesseRepository witRepository;
       private ISuspectRepositroy susRepository;
       private IEvidenceRepository evidenceRepositroy;
       private IAccountRepository accountRepository;
       public CaseController()
       {
           this.menuRepository = RepositoryFactory.MakeCaseRepository(GetDBContext());
           this.witRepository = RepositoryFactory.MakeWitnesseRepository(GetDBContext());
           this.susRepository = RepositoryFactory.MakeSuspectRepositroy(GetDBContext());
           this.evidenceRepositroy = RepositoryFactory.MakeEvidenceRepository(GetDBContext());
           this.accountRepository = RepositoryFactory.MakeAccountRepositroy(GetDBContext());
       }

       public void CreateCase(string password)
       {
           NewCase nc = new NewCase();
           accountRepository.Load(password).myCases.Add(nc);
           
           menuRepository.Add(nc);
           menuRepository.Commit();
           accountRepository.Commit();
       }
       public NewCase[] GetAllCases()
       {
           return menuRepository.LoadAll().ToArray();
       }
       
        public NewCaseView OpenCase(long id)
       {
           var rep = menuRepository.LoadById(id);
           return ViewModelFactory.BuildNewCaseView(rep);
       }
       public void AddWitnesse(string firstName,string lastName,long id)
       {
           var wit = new Witnesses();
           wit.firstName = firstName;
           wit.lastName = lastName;
           menuRepository.AttachByID(id).AddWitnesse(wit);
           menuRepository.Commit();
           
       }
       public void AddSuspect(string firstName,string lastName,long id)
       {
           var sus = new Suspect();
           sus.firstName = firstName;
           sus.lastName = lastName;
           menuRepository.AttachByID(id).AddSuspect(sus);
           menuRepository.Commit();

       }
        public void AddEvidence(string information,string typeDetail,string urlphoto,long id)
       {
           var ev = new Evidence();
           ev.information = information;
           ev.evidenceType = typeDetail;
           ev.urlPhoto = urlphoto;
           menuRepository.AttachByID(id).AddEvidence(ev);
           menuRepository.Commit();
       }
       public void AddWitnesseFrom( long id,long witnesseID)
       {
           menuRepository.AttachByID(id).Witnesse_List.Add(witRepository.LoadById(witnesseID));
           menuRepository.Commit();   
       }
       public void AddSuspectFrom(long id, long suspectID)
       {
           menuRepository.AttachByID(id).Suspect_List.Add(susRepository.LoadById(suspectID));
           menuRepository.Commit();
       }
        public void AddEvidenceFrom(long id,long evidenceID)
       {
           menuRepository.AttachByID(id).Evidence_List.Add(evidenceRepositroy.LoadById(evidenceID));
           menuRepository.Commit();
       }
        private Witnesses FindWitnesse(long id)
       {
           return ControllerUtils.TakeObjectById(witRepository, id);
       }
        private NewCase FindNewCase(long id)
        {
            return ControllerUtils.TakeObjectById(menuRepository, id);
        }


    }
}
