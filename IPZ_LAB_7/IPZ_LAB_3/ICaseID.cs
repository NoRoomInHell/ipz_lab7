﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ_LAB_3
{
    interface ICaseID
    {
        long  ID{get;set;}
        string infoDetail { get; set; }

    }
}
