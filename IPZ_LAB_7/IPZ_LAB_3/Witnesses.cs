﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ_LAB_3
{
   public class Witnesses
    {
       public long currentIdCase { get; set; }

       public string firstName{get;set;}
       public string lastName { get; set; }
       public  string addres { get; set; }

        public Witnesses()
        {
            Name name = new Name();
            this.firstName = name.FirstName;
            this.lastName = name.LastName;
            IDHolder(currentIdCase);
        }
        public void ShowFullInfo()
        {
            Console.WriteLine("Witnesse's Full name is: " + firstName + " " + firstName + " ");
            Console.WriteLine("Witnesse's Full address is: " + addres + " " + addres + " " + addres + " ");
            Console.WriteLine("ID is : " + currentIdCase);
        }
        public override string ToString()
        {

            return "Witnesse's Full name is: " + firstName.ToString() + " " + lastName.ToString() + " \n" 
                + "Witnesse ID: " + currentIdCase +"\n";
        }

        public long IDHolder(long id)
        {
            currentIdCase = id;
            return currentIdCase;
        }
       
      
        
       

    }
}
