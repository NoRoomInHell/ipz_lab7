﻿using System;
using System.IO;
using IPZ_LAB_3;
using IPZ_LAB_3.Orm;
using IPZ_LAB_3.ProkController;
using IPZ_LAB_3.СlientConsole.Commands;
namespace IPZ_LAB_3.ProkControllerAP
{
    class Program
    {
        static void Main(string[] args)
       {
           // TestAccData();
           //TestAcc();
             
        RunCommandProccesingLoop(System.Console.In, System.Console.Out);
          
        }
        private static void AccountTest(IAccountController accContr)
        {
          
            accContr.SignIn("1234");
         
        }
        private static void AccountData(IAccountDataController accData)
        {
            
            accData.CreateAccount("1234", "Glab", "Vedm");
            accData.CreateAccount("acc", "Jon", "Pigon");
        }
        private static void SignInAccount()
        {
            using(var accContr = ControllerFactory.MakeAccountController())
            {
                AccountTest(accContr);
            }
        }
        private static void CreateAccount()
        {
            using (var accContrData = ControllerFactory.MakeAccountDataController())
            {
                AccountData(accContrData);
            }
        }
        private static void RunCommandProccesingLoop(TextReader input,TextWriter output)
        {
            CommandHandler handler = new CommandHandler();
            InitCommands(handler, output);
            while(true)
            {
                output.Write("> ");
                string command = input.ReadLine();
                try
                {
                    handler.ProcessCommandLine(command);
                }
                catch(Exception e)
                {
                    output.WriteLine(string.Format("Error:{0}\n{1}\n", e.Message, (e.InnerException != null) ? e.InnerException.Message
                        : ""));
                }
            }
        }

        
        private static void AddWitnesse(IWitnesseController witnesController)
        {
          
            witnesController.CreateNewWitnesse("TESTNAME", "TESTLASTNAME");
            witnesController.CreateNewWitnesse("LOL", "RIOT");
            witnesController.CreateNewWitnesse("TEST 3", " 3 TEST");
            witnesController.CreateNewWitnesse("TEST 4 ", "4 TEST");
            witnesController.CreateNewWitnesse("TEST 5", "5 TEST");
            witnesController.CreateNewWitnesse("TEST 6", "6 TEST");
        }
        private static void AddEvidence(IEvidenceController evidenceController)
        {
            evidenceController.CreateEvidence("Some info 1", "Some facts and detail 1", "http://quetel.com/sites/default/files/Evidence_1.jpg");
            evidenceController.CreateEvidence("Some info 2", "Some facts and detail 2", "http://quetel.com/sites/default/files/Evidence_1.jpg");
            evidenceController.CreateEvidence("Some info 3", "Some facts and detail 3", "http://quetel.com/sites/default/files/Evidence_1.jpg");
        }
        private static void AddSuspect(ISuspectController susController)
        {

            susController.CreateNewSuspect("SuspectName1", "SuspectLastName1");
            susController.CreateNewSuspect("SuspectName2", "SuspectLastName2");
            susController.CreateNewSuspect("SuspectName3", "SuspectLastName3");
            susController.CreateNewSuspect("SuspectName4", "SuspectLastName4");
        
        }
 
        private static void CreateWitnesse()
        {
            using(var witController = ControllerFactory.MakeWitnesseController())
            {
              AddWitnesse(witController);
               

             
            }
        }
        private static void CreateSuspect()
        {
            using(var susController = ControllerFactory.MakeSuspectController())
            {
                AddSuspect(susController);
            }
        }
        private static void CreateEvidence()
        {

            using(var evController = ControllerFactory.MakeEvidenceController())
            {
                AddEvidence(evController);
            }
        }
         
         
                public static void InitCommands(CommandHandler handler,TextWriter output)
        {
            handler.RegisterCommand(new CreateWitnesseCommand(output));
            handler.RegisterCommand(new FindWitnesseCommand(output));
            handler.RegisterCommand(new HelpCommand(handler, output));
            handler.RegisterCommand(new WitnesseEditCommand(output));
            handler.RegisterCommand(new FindWitnesseByIDCommand(output));
            handler.RegisterCommand(new NewCaseCommand(output));
            handler.RegisterCommand(new NewCaseAddWitnesseCommand(output));
            handler.RegisterCommand(new NewCaseAddSuspectFrom(output));
            handler.RegisterCommand(new NewCaseAddWitnesseFromBase(output));
            handler.RegisterCommand(new NewCaseShowCommand(output));
            handler.RegisterCommand(new CreateSuspectCommand(output));
            handler.RegisterCommand(new FindSuspectCommand(output));
            handler.RegisterCommand(new SuspectEditCommand(output));
            handler.RegisterCommand(new SuspectsShowCommand(output));
            handler.RegisterCommand(new WitnesseShowCommand(output));
            handler.RegisterCommand(new NewCaseAddEvidenceCommand(output));
            handler.RegisterCommand(new AccountSignInShow(output));
            handler.RegisterCommand(new CreateNewAccountCommand(output));
            handler.RegisterCommand(new FindSuspectByID(output));
            handler.RegisterCommand(new QuitCommand(output));
        }
        
    }
}
