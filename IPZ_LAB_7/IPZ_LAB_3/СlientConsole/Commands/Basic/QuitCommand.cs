﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace IPZ_LAB_3.СlientConsole.Commands
{
    class QuitCommand:Command
    {
        public QuitCommand(TextWriter output)
            : base("quit", output)
        {
        }


        public override void Execute(CommandSwitchValue values)
        {
            Output.WriteLine("Good Bye!");
           // Account.signIn = false;
            Environment.Exit(0);
        }
    }
}
