﻿using System;

namespace IPZ_LAB_3.СlientConsole.Commands
{
    class CommandSwitch
    {
        public string Name { get; private set; }
        public bool Optional { get; private set; }
        public ValueMode Mode { get; private set; }
        public enum ValueMode
        {
            Unexpected,
            ExpectedSingle,
            ExpectedMultiple
        };

        public CommandSwitch(string name,ValueMode mode,bool optional)
        {
            if (name.Length == 0)
                throw new Exception("CommandSwitch: Name is empty, fill the name cell");
            this.Name = name;
            this.Mode = mode;
            this.Optional = optional;
        }
    }
}
