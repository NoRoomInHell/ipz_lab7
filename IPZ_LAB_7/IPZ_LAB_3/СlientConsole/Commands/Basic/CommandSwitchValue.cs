﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IPZ_LAB_3.СlientConsole.Commands
{
    class CommandSwitchValue
    {
        public IDictionary<string,string> values;
        public CommandSwitchValue()
        {
            this.values = new Dictionary<string, string>();
        }
        public bool HasSwitch(string swtichName)
        {
            return values.ContainsKey(swtichName);
        }

        public void SetSwitch(string switchName, string switchValue)
        {
            values.Add(switchName, switchValue);

        }
        public void AppendSwitch(string switchName,string switchValue)
        {
            string exisitingValue;
            if (values.TryGetValue(switchName, out exisitingValue))
            {
                string extendValue = exisitingValue + ' ' + switchValue;
                values.Remove(switchName);
                values.Add(switchName, extendValue);

            }
            else
                values.Add(switchName, switchValue);
        }
        public string GetSwitch(string switchName)
        {
            string switchValue;
            if (values.TryGetValue(switchName, out switchValue))
                return switchValue;

            else
                throw new Exception("Switch " + switchName + " was not specified.");
        }


        public int GetSwitchAsInt(string switchName)
        {
            string switchValue = GetSwitch(switchName);

            int result;
            if (Int32.TryParse(switchValue, out result))
                return result;

            throw new Exception("Switch " + switchName + " must have an integer value.");
        }

        public long GetSwitchAsLong(string switchName)
        {
            string switchValue = GetSwitch(switchName);
            long result;
            if (Int64.TryParse(switchValue, out result))
                return result;
            throw new Exception("Switch " + switchName + " must have an integer value.");
        }
        public double GetSwitchAsDouble(string switchName)
        {
            string switchValue = GetSwitch(switchName);

            double result;
            if (Double.TryParse(switchValue, out result))
                return result;

            throw new Exception("Switch " + switchName + " must have a real value.");
        }


        public decimal GetSwitchAsDecimal(string switchName)
        {
            string switchValue = GetSwitch(switchName);

            decimal result;
            if (Decimal.TryParse(switchValue, out result))
                return result;

            throw new Exception("Switch " + switchName + " must have a decimal value.");
        }


        public bool GetSwitchAsBool(string switchName)
        {
            string switchValue = GetSwitch(switchName);

            bool result;
            if (Boolean.TryParse(switchValue, out result))
                return result;

            throw new Exception("Switch " + switchName + " must have a value 'true' or 'false'.");
        }


        public string GetOptionalSwitch(string switchName)
        {
            string switchValue;
            if (values.TryGetValue(switchName, out switchValue))
                return switchValue;

            else
                return null;
        }
        

    }
}
