﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace IPZ_LAB_3.СlientConsole.Commands
{
    class HelpCommand: Command
  {
       public HelpCommand ( CommandHandler handler, TextWriter output )
            :   base( "help", output )
        {
            AddSwitch( new CommandSwitch( "-command", CommandSwitch.ValueMode.ExpectedSingle, true ) );

            this.handler = handler;
        }


        public override void Execute ( CommandSwitchValue values )
        {
            string commandName = values.GetOptionalSwitch( "-command" );
            if ( commandName != null )
            {
                Command command = handler.FindCommand( commandName );
                if ( command == null )
                    throw new Exception( "help: command " + commandName + " not found" );

                ShowHelpFor( command );
            }

            else
            {
               foreach ( string name in handler.CommandNames )
                   ShowHelpFor( handler.FindCommand( name ) );
            }
        }


        private void ShowHelpFor ( Command command )
        {
            Output.Write( command.Name );
            foreach ( string switchName in command.SwitchNames )
            {
                Output.Write( ' ' );

                CommandSwitch sw = command.FindSwitch( switchName );

                if ( sw.Optional )
                    Output.Write( '[' );

                if ( sw.Mode == CommandSwitch.ValueMode.ExpectedMultiple )
                    Output.Write( '{' );

                Output.Write( switchName );

                if ( sw.Mode != CommandSwitch.ValueMode.Unexpected )
                    Output.Write( " <value>" );

                if ( sw.Mode == CommandSwitch.ValueMode.ExpectedMultiple )
                    Output.Write( '}' );

                if ( sw.Optional )
                    Output.Write( ']' );
            }
            Output.WriteLine();
        }


        private CommandHandler handler;
    }
}
