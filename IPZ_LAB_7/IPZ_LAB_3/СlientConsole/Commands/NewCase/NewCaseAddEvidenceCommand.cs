﻿using System;
using System.Collections.Generic;
using System.IO;
using IPZ_LAB_3.ProkController;

namespace IPZ_LAB_3.СlientConsole.Commands
{
    class NewCaseAddEvidenceCommand:Command
    {
        public NewCaseAddEvidenceCommand(TextWriter output)
            :base("case.addEvidence",output)
        {
            AddSwitch(new CommandSwitch("-info",CommandSwitch.ValueMode.ExpectedSingle,true));
            AddSwitch(new CommandSwitch("-type", CommandSwitch.ValueMode.ExpectedSingle, true));
            AddSwitch(new CommandSwitch("-photo", CommandSwitch.ValueMode.ExpectedSingle, true));
            AddSwitch(new CommandSwitch("-ID", CommandSwitch.ValueMode.ExpectedSingle, true));

        }
        public override void Execute(CommandSwitchValue values)
        {
            string info = values.GetOptionalSwitch("-info");
            string type = values.GetOptionalSwitch("-type");
            string photo = values.GetOptionalSwitch("-photo");
            long ID = values.GetSwitchAsLong("-ID");
            using(var caseController = ControllerFactory.MakeCaseController())
            {
                caseController.AddEvidence(info, type, photo,ID);
            }
        }
    }
}
