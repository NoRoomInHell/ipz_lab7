﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class NewCaseShowCommand:Command
    {
        public NewCaseShowCommand(TextWriter output)
            :base("case.show",output)
        {
            AddSwitch(new CommandSwitch("-ID", CommandSwitch.ValueMode.ExpectedSingle, false));
        }
        public override void Execute(CommandSwitchValue values)
        {
            long ID = values.GetSwitchAsLong("-ID");
            using (var caseController = ControllerFactory.MakeCaseController())
            {
               Output.Write(caseController.OpenCase(ID));
            }
        }
    }
}
