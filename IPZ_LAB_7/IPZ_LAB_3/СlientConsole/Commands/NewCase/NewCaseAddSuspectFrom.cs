﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class NewCaseAddSuspectFrom:Command
    {
        public NewCaseAddSuspectFrom(TextWriter output)
            :base("case.addSusFrom",output)
        {
            AddSwitch(new CommandSwitch("-caseID", CommandSwitch.ValueMode.ExpectedSingle, false));
            AddSwitch(new CommandSwitch("-suspectID", CommandSwitch.ValueMode.ExpectedSingle, false));
        }
        public override void Execute(CommandSwitchValue values)
        {
            long idCase = values.GetSwitchAsLong("-caseID");
            long idSuspect = values.GetSwitchAsLong("-suspectID");

            using(var caseController = ControllerFactory.MakeCaseController())
            {
                caseController.AddSuspectFrom(idCase, idSuspect);
            }
        }
    }
}
