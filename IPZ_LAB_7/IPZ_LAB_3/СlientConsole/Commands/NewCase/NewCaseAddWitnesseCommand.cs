﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class NewCaseAddWitnesseCommand:WitnesseCommand
    {
        public NewCaseAddWitnesseCommand(TextWriter output)
        :base("case.addWitnesse",output)
        {
           
        }
        public override void Execute(CommandSwitchValue values)
        {
            string name = GetName(values);
            string lastName = GetLastName(values);
            long ID = GetId(values);
            using(var caseController = ControllerFactory.MakeCaseController())
            {
                caseController.AddWitnesse(name, lastName, ID);
            }


        }
    }
}
