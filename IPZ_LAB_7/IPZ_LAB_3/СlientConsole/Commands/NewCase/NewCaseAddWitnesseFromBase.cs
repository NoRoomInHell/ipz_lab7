﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class NewCaseAddWitnesseFromBase:Command
    {
        public NewCaseAddWitnesseFromBase(TextWriter output)
            :base("case.addWitFrom",output)
        {
            AddSwitch(new CommandSwitch("-caseID", CommandSwitch.ValueMode.ExpectedSingle, false));
            AddSwitch(new CommandSwitch("-witnesseID", CommandSwitch.ValueMode.ExpectedSingle, false));
        }
        public override void Execute(CommandSwitchValue values)
        {
            long idCase = values.GetSwitchAsLong("-caseID");
            long idWitnesse = values.GetSwitchAsLong("-witnesseID");

            using(var caseController = ControllerFactory.MakeCaseController())
            {
                caseController.AddWitnesseFrom(idCase, idWitnesse);
            }
        }
    }
}
