﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
     class NewCaseCommand:Command
    {
        
        public NewCaseCommand(TextWriter output)
            :base("case.create",output)
        {
            AddSwitch(new CommandSwitch("-password", CommandSwitch.ValueMode.ExpectedSingle, false));
            
        }
        public override void Execute(CommandSwitchValue values)
        {
            string password = values.GetSwitch("-password");
            using (var caseController = ControllerFactory.MakeCaseController())
            {
                caseController.CreateCase(password);
            }
        }
    }
}
