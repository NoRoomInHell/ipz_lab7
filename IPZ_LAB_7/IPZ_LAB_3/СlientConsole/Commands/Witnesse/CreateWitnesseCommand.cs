﻿using System;
using IPZ_LAB_3.ProkController;
using System.IO;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class CreateWitnesseCommand:Command
    {
        public CreateWitnesseCommand(TextWriter output)
            : base("witnesse.create", output)
        {
            AddSwitch(new CommandSwitch("-name", CommandSwitch.ValueMode.ExpectedSingle, true));
            AddSwitch(new CommandSwitch("-lastName", CommandSwitch.ValueMode.ExpectedSingle, true));
           
        }
        public override void Execute(CommandSwitchValue values)
        {
            
            string name = values.GetSwitch("-name");
            string lastName = values.GetSwitch("-lastName");
         
            using(var witContr = ControllerFactory.MakeWitnesseController())
            {
                witContr.CreateNewWitnesse(name, lastName);
            }
        }
    }
}
