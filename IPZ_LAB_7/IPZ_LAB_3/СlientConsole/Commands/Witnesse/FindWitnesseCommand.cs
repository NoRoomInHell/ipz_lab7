﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class FindWitnesseCommand:WitnesseCommand
    {
        public FindWitnesseCommand(TextWriter output)
         : base("witnesse.find",output)
    {

    }
        public override void Execute(CommandSwitchValue values)
        {
            string firstName = GetName(values);
            
            string lastName = GetLastName(values);
            
            using(var witController = ControllerFactory.MakeWitnesseController())
            {
                Output.WriteLine(witController.FindWitnesse(firstName,lastName));
            }
        }
    }
}
