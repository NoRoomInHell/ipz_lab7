﻿using System;
using System.IO;
namespace IPZ_LAB_3.СlientConsole.Commands
{
   abstract class WitnesseCommand:Command
    {
       protected WitnesseCommand(string name,TextWriter output)
           :base(name,output)
       {
           AddSwitch(new CommandSwitch("-firstName", CommandSwitch.ValueMode.ExpectedSingle, true));
           AddSwitch(new CommandSwitch("-lastName", CommandSwitch.ValueMode.ExpectedSingle, true));
           AddSwitch(new CommandSwitch("-ID", CommandSwitch.ValueMode.ExpectedSingle, true));
       }

       protected string GetName(CommandSwitchValue values)
       {
           return values.GetOptionalSwitch("-firstName");
       }
       protected string GetLastName(CommandSwitchValue values)
       {
           return values.GetOptionalSwitch("-lastName");
       }
       protected long GetId(CommandSwitchValue values)
       {
           return values.GetSwitchAsLong("-ID");
       }
       
    }
}
