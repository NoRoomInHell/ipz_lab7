﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class WitnesseShowCommand:Command
    {
        public WitnesseShowCommand(TextWriter output)
            :base("witnesse.show",output)
        { }

        public override void Execute(CommandSwitchValue values)
        {
           using(var witController = ControllerFactory.MakeWitnesseController())
           {
               Output.Write(witController.GetAllWitnesses());
           }
        }
    }
}
