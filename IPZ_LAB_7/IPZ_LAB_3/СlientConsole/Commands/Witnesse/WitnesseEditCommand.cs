﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;

namespace IPZ_LAB_3.СlientConsole.Commands
{
    class WitnesseEditCommand:Command
    {

        public WitnesseEditCommand(TextWriter output)
            :base("witnesse.rename",output)
        {
            AddSwitch(new CommandSwitch("-newName", CommandSwitch.ValueMode.ExpectedSingle, false));
            AddSwitch(new CommandSwitch("-newLastName", CommandSwitch.ValueMode.ExpectedSingle, false));
            AddSwitch(new CommandSwitch("-ID", CommandSwitch.ValueMode.ExpectedSingle, false));
        }
        public override void Execute(CommandSwitchValue values)
        {
            string newName = values.GetSwitch("-newName");
            string newLastName = values.GetSwitch("-newLastName");
            long ID = values.GetSwitchAsLong("-ID");
           using(var witController = ControllerFactory.MakeWitnesseController())
           {
               witController.Rename(ID, newName, newLastName);
           }
        }

    }
}
