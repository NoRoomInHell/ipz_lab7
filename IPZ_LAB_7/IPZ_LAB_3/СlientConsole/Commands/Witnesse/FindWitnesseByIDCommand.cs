﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class FindWitnesseByIDCommand:Command
    {
        public FindWitnesseByIDCommand(TextWriter output)
            :base("witnesse.findID",output)
        {
            AddSwitch(new CommandSwitch("-ID", CommandSwitch.ValueMode.ExpectedSingle, false));

        }
        public override void Execute(CommandSwitchValue values)
        {
            long ID = values.GetSwitchAsLong("-ID");
            using(var witnesseController = ControllerFactory.MakeWitnesseController())
            {
             Output.Write(witnesseController.FindWitnesseByID(ID));
            }
        }
    }
}
