﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class FindSuspectByID:Command
    {
        public FindSuspectByID(TextWriter output)
            :base("suspect.findID",output)
        {
            AddSwitch(new CommandSwitch("-ID", CommandSwitch.ValueMode.ExpectedSingle, false));
        }
        public override void Execute(CommandSwitchValue values)
        {
            long id = values.GetSwitchAsLong("-ID");
            using(var susController = ControllerFactory.MakeSuspectController())
            {
                Output.Write(susController.FindSuspectByID(id));
            }
            }
    }
}
