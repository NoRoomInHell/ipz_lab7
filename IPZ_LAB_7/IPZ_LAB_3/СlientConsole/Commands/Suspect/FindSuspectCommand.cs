﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class FindSuspectCommand:SuspectCommand
    {
           public FindSuspectCommand(TextWriter output)
         : base("suspect.find",output)
    {

    }
        public override void Execute(CommandSwitchValue values)
        {
            string firstName = GetName(values);
            
            string lastName = GetLastName(values);
            
            using(var susController = ControllerFactory.MakeSuspectController())
            {
                Output.WriteLine(susController.FindSuspect(firstName,lastName));
            }
        }
    }
    }

