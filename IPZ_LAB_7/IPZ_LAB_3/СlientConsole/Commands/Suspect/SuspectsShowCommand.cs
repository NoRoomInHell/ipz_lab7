﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class SuspectsShowCommand:Command
    {
        public SuspectsShowCommand(TextWriter output)
            :base("suspect.show", output)
        {

        }
        public override void Execute(CommandSwitchValue values)
        {
            using(var susController = ControllerFactory.MakeSuspectController())
            {
                Output.Write(susController.GetAllSuspect());
            }
        }
    }
}
