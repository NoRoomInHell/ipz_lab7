﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class CreateSuspectCommand:SuspectCommand
    {
        public CreateSuspectCommand(TextWriter output)
            : base("suspect.create", output)
        {
           
        }
        public override void Execute(CommandSwitchValue values)
        {

            string name = GetName(values);
            string lastName = GetLastName(values);
         
            using(var witContr =ControllerFactory.MakeSuspectController())
            {
                witContr.CreateNewSuspect(name, lastName);
            }
        }
    }
    }

