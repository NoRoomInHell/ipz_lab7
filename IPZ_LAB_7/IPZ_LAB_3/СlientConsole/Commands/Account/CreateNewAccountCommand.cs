﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class CreateNewAccountCommand:Command
    
    {
        public CreateNewAccountCommand(TextWriter output)
            :base("account.create",output)
        {
            AddSwitch(new CommandSwitch("-password", CommandSwitch.ValueMode.ExpectedSingle, false));
            AddSwitch(new CommandSwitch("-name", CommandSwitch.ValueMode.ExpectedSingle,false));
            AddSwitch(new CommandSwitch("-lastName",CommandSwitch.ValueMode.ExpectedSingle,false));
        }
        public override void Execute(CommandSwitchValue values)
        {
            string pass = values.GetSwitch("-password");
            string name = values.GetSwitch("-name");
            string lastName = values.GetSwitch("-lastName");
            using(var accDataController = ControllerFactory.MakeAccountDataController())
            {
                accDataController.CreateAccount(pass, name, lastName);
            }
        }
    }
}
