﻿using System;
using System.IO;
using IPZ_LAB_3.ProkController;
namespace IPZ_LAB_3.СlientConsole.Commands
{
    class AccountSignInShow:Command

    {
        public AccountSignInShow(TextWriter output)
            :base("signIn",output)
        {
            AddSwitch(new CommandSwitch("-password", CommandSwitch.ValueMode.ExpectedSingle, false));

        }
        public override void Execute(CommandSwitchValue values)
        {
            string password = values.GetSwitch("-password");
            using (var accContr = ControllerFactory.MakeAccountController())
            {
               Output.Write(accContr.SignIn(password));
            }
        }
    }
}
